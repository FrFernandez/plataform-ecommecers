import { Elements } from '../../utils';
import { Users as UsersModels } from '../../models';

class Users extends Elements {
  constructor(){ 
    super();
    
    this.count        = this.count.bind(this);
    this.find         = this.find.bind(this);
    this.findOne      = this.findOne.bind(this);
    this.findById     = this.findById.bind(this);
    this.findAll      = this.findAll.bind(this);
    this.create       = this.create.bind(this);
    this.updateById   = this.updateById.bind(this);
    this.delete       = this.delete.bind(this);

    this.users    = UsersModels;
  } 

  async count(query = {}, fn){
    let doc;
    
    try {
      doc = await this.users.count(query);

      return fn ? fn(false, doc) : doc;
    } catch(err) {
      return fn ? fn(err, false) : err; 
    }
  }

  async find(query = {}, fields = null, options = {}, fn){
    let docs;

    try {
      docs = await this.users.find(query, fields, options);

      return fn ? fn(false, docs) : docs;
    } catch(err) {
      return fn ? fn(err, false) : err;
    }
  } 

  async findOne(query = {}, fields = {}, options = {}, fn){
    let doc;

    try {
      doc = await this.users.findOne(query, fields, options);

      return fn ? fn(false, doc) : doc;
    } catch(err) {
      return fn ? fn(err, false) : err;
    }
  }

  async findById(id, fields = {}, options = {}, fn){
    if(this.isValid(id))
      return fn ? fn(this.handlerError('ID'), false) : this.handlerError('ID');

    let doc;

    id = this.isValid(id);
    
    try {
      doc = await this.users.findById(id, fields, options);

      return fn ? fn(false, doc) : doc;
    } catch(err) {
      return fn ? fn(err, false) : err;
    }
  }

  async findAll(query = {}, fields = {}, options = {}, fn){
    
  }

  async create(body = {}, fn){
    let doc;

    try {
      doc = await this.users.create(body);
      
      return fn ? fn(false, doc) : doc;
    } catch(err) {
      return fn ? fn(err, false) : err;
    } 
  }

  async updateById(id, body = {}, options = {}, fn){
    if(!this.isValid(id))
      return fn ? fn(this.handlerError('ID'), false) : this.handlerError('ID');

    let doc;

    id = this.isValid(id);

    try {
      doc = await this.users.updateOne({  _id: id }, body, options);
    
      return fn ? fn(false, doc) : doc;
    } catch(err) {
      return fn ? fn(err, false) : err;
    }
  }

  async delete(id, options = {}, fn){
    if(!this.isValid(id))
      return fn ? fn(this.handlerError('ID'), false) : this.handlerError('ID');

    let doc;

    id = this.isValid(id);

    try {
      doc = await this.users.findByIdAndRemove(id, options);

      return fn ? fn(false, doc) : doc;
    } catch(err) {
      return fn ? fn(err, false) : err;
    }
  }

}

export default Users;