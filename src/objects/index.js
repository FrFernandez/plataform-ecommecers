import Auth from './auth';
import Categories from './categories';
import Comments from './comments';
import Departments from './departments';
import Likes from './likes';
import Messages from './messages';
import Notifications from './notifications';
import Publications from './publications';
import Reviews from './reviews';
import Users from './users';
import Views from './views';

export {
  Auth,
  Categories,
  Comments,
  Departments,
  Likes,
  Messages,
  Notifications,
  Publications,
  Reviews,
  Users,
  Views
}
