import Mongoose from 'mongoose';

const ObjectId = Mongoose.Types.ObjectId;

const pattern = [
  ' ','Š','Œ','Ž','š','œ','ž','Ÿ','¥','µ','À','Á','Â','Ã','Ä','Å','Æ','Ç',
  'È','É','Ê','Ë','?','Ì','Í','Î','Ï','I','Ð','Ñ','Ò','Ó','Ô','Õ','Ö','Ø',
  'Ù','Ú','Û','Ü','Ý','ß','à','á','â','ã','ä','å','æ','ç','è','é','ê','ë',
  '?','ì','í','î','ï','i','ð','ñ','ò','ó','ô','õ','ö','ø','ù','ú','û','ü',
  'ý','ÿ'
];
const pattern_replace = [
  '','S','O','Z','s','o','z','Y','Y','u','A','A','A','A','A','A','A','C',
  'E','E','E','E','E','I','I','I','I','I','D','N','O','O','O','O','O','O',
  'U','U','U','U','Y','s','a','a','a','a','a','a','a','c','e','e','e','e',
  'e','i','i','i','i','i','o','n','o','o','o','o','o','o','u','u','u','u',
  'y','y'
];

export default class Elements {

  isValid(value) {
    switch(typeof value) {
      case 'string':
        if(ObjectId.isValid(value))
          return new ObjectId(value);
        else 
          return false;
      case 'object':
        if(ObjectId.isValid(value))
          return new ObjectId(value);
        else 
          return Object.keys(value).length > 0 ? true : false;
      case 'array':
        return value.length > 0 ? true : false;
      case 'function':
        return true;
      default: 
        return false;
    }
  }

  pattern(t, v) {
    switch(type) {
      case 'email':
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(v);
      case 'phone':
        return /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/.test(v);
      case 'mobile':
        return /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/.test(v);
      case 'date':
        return /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/.test(v);
    }
  }

  clean(string) {
    return string.replace(
      new RegExp(`(${
          (
            typeof(pattern) === 'string'
              ?
            pattern.replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&")
              :
            pattern.map(i => i.replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&")).join('/')
          )
        })`,
        'g' 
      ),
      typeof(pattern_replace) === 'string'
        ?
      pattern_replace
        : 
      typeof(pattern) === 'string'
        ?
      pattern_replace[0]
        :
      i => pattern_replace[pattern.indexOf(i)]
    )
  }    

  switch (key) {
    switch(key) {
      case 'ID':
        return 'is not valid Id or not send Id';
      default:
        return 'Óops, ocurrio un error interno.';
    }
  }

  handlerError(error) {

    return this.switch(error);
  }
}