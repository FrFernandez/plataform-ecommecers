import fs from 'fs';
import path from 'path';
import Multer from 'multer';
import config from '../../../config';
import Elements from '../elements';

const STORAGE_URL = config.STORAGE_URL;
const tmp         = path.resolve(__dirname, `../../../${config.TMP_DIR}`);
const storage     = path.resolve(__dirname, `../../../${config.STORAGE_DIR}`);

const multer_config = Multer.diskStorage({
  destination: (req, file, next) => next(null, tmp) ,
  filename: (req, file, next) => next(null, file.originalname)
})

export default class Uploads extends Elements {
  constructor() {
    super();

    this.file = {};
    this.uris = {};
    this.multer = Multer({ storage: multer_config });
  }
  single(param) {

    return this.multer.single(param);
  }

  fields(param) {

    return this.multer.fields(param);
  }

  array(param) {

    return this.multer.array(param);
  } 

  getformat(file) {
    if (this.isValid(this.file))
      return this.file = {
        ...this.file,
        format: this.file.mimetype.split('/')[1]
      }
    else if(this.isValid(file))
      return file.mimetype.split('/')[1];
    else 
      return false;
  }

  getPath(name, paths) {
    if (this.isValid(this.file)) {
      this.getformat();
      
      return this.file = {
        ...this.file,
        storage: path.resolve(storage, `${paths}/${name}.${this.file.format}`)
      }
    }else return false;      
  }

  getUri(path, name) {
    return `${config.STORAGE_URL}/${path}/${name}.${this.file.format}`;
  }

  createFolder(directory, cb) {
    if (!this.isValid(cb))
      throw 'cb is not a function or not send callback';

    let paths = path.resolve(storage, directory);

    return fs.mkdir(paths, cb);
  }

  move(cb) {
    if (!this.isValid(cb)) 
      throw 'cb is not a function or not send callback';

    if (this.isValid(this.file))
      return fs.rename(this.file.path, this.file.storage, cb)
    else 
      return cb('Not file exist');
  }

  exist(paths, position, cb) {
    if (!this.isValid(cb)) 
      throw 'cb is not a function or not send callback';

    let parts     = path.normalize(paths).split('\\');
        position  = position || 0;  

    if (position >= parts.length){
      let dr = parts.join('/');
      
      if (cb)
        return cb(null, dr);
      else 
        return dr;
    }

    let directory = parts.slice(0, position + 1).join('/');

    return fs.stat(path.resolve(storage , directory), async err => {
      if (err === null) {
        return this.exist(paths, position + 1, cb);
      }else {
        return this.createFolder(directory,  async err => {
          if (err){
            if (cb)
              return cb(err, null);
            else 
              throw err;
          }else return this.exist(paths, position + 1, cb);
        });
      }
    })  
  }

  upload(destination, name, file, cb) {
    if (!this.isValid(cb))
      throw 'cb is not a function or not send callback';
    if (!destination) 
      throw 'Not send destination';
    if (!this.isValid(file)) 
      return cb('Not send file', null);

    return this.exist(destination, null, (err, path) => {
      if(err) 
        return cb(err, null);

      this.file = file;

      this.getPath(name, path); 
      
      return this.move(err => {
        if(err) 
          return cb(err, null);

        return cb(null, this.getUri(destination, name));
      });
    })
  }

  processUpload(destination, name, files, dir, cb) {
    destination = `${destination}/${dir}`;
    name = `${name}_${Date.now()}`;

    let uris = [];

    for (const key in files) {
      this.upload(destination, name, files[key], (err, uri) => err ? cb(err) : false);
      console.log(this.getformat(files[key]))
      uris.push({
        uri: `${destination}/${name}.${this.getformat(files[key])}`,
        url: `${STORAGE_URL}/cdn/${destination}/${name}.${this.getformat(files[key])}`
      });
    }
    return cb(null, uris);
  }

  async uploads(destination, name, files, cb, dir = null) {
    if(!this.isValid(cb))
      throw 'cb is not a function or not send callback';
    if(!destination)
      throw 'Not send destination';
    if(!this.isValid(files))
      return cb('Not send files', null);
      
    if (!Array.isArray(files) && dir === null) {  
       return await this.exist(destination, null, async () => {
        for (const key in files) {
          if (typeof key === 'string') {
            await this.uploads(destination, name, files[key], (err, uri) => err ? cb(err, null) : false, key);
          }
          else return cb('error_2', null);
        }
        return cb(null, this.uris); 
      });
    }else {
      await this.processUpload(destination, name, files, dir, (err, uri) => {
        this.uris[dir] = uri;
        return cb(null, uri);
      })
    }
  }
}

