import { db } from '../../utils';

const Schema = db.Schema;

export default db.model('reviews', new Schema({
	name: String
}));