import { db } from '../../utils';

const Schema = db.Schema;

export default db.model('likes', new Schema({
	name: String
}));