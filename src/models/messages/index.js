import { db } from '../../utils';

const Schema = db.Schema;

export default db.model('messages', new Schema({
	name: String   
}));