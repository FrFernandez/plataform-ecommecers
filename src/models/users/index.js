import { db } from '../../utils';
import bcrypt from 'bcryptjs';
import schortid from 'shortid';
import mongoose from '../../utils/db';

const Schema   = db.Schema;
const ObjectId = mongoose.Types.ObjectId;

const UserSchema = new Schema({
  miniId: {
    type: String,
    default: schortid.generate
  },
  email: {
    type: String,
    required: true,
    unique: true,
    trim: true
  },
  username: {
    type: String,
    lowercase: true,
    unique: true,
    required: true
  },
  password: String,
  name: {
    type: String,
    required: true
  },
  lastname: {
    type: String,
    required: true
  },
  displayName: {
    type: String,
    lowercase: true,
    trim: true
  },
  dni: {
    type: String,
    unique: true
  },
  gender: Number, // gender male or female and other (1|2|3)
  mobile: {
    type: String,
    unique: true
  },
  phone: String,
  address: String,
  birthday: Date, 
  photo: Object,
  picture: Object,
  cover: Object,
  localities: [
    {
      type: Schema.Types.ObjectId,
      ref: 'localities'
    }
  ],
  website: String,
  rating: {
    seller: {
      tallied: {
        zero: {
          type: Number,
          default: 0
        },
        one: {
          type: Number,
          default: 0
        },
        two: {
          type: Number,
          default: 0
        },
        three: {
          type: Number,
          default: 0
        }
      },
      total: {
        type: Number,
        default: 0
      },
      totalTallied: {
        type: Number,
        default: 0
      }
    },
    buyer: {
      tallied: {
        zero: {
          type: Number,
          default: 0
        },
        one: {
          type: Number,
          default: 0
        },
        two: {
          type: Number,
          default: 0
        },
        three: {
          type: Number,
          default: 0
        }
      },
      total: {
        type: Number,
        default: 0
      },
      totalTallied: {
        type: Number,
        default: 0
      }
    }
  },
  state: { // register state complete or incomplete (true|false) 
    type: Boolean,
    default: false
  },
  verify: { // User not verify by email 
    type: Boolean,
    default: false
  }, 
  status: { // User enabled or disable and lock (1|2|3) 
    type: Number,
    default: 1
  },
  online: {
    type: Boolean,
    default: false
  },
  priv: { // Priv user, default 3 for users current, 2 for users workers, 1 for users root 
    type: Number,
    default: 3
  },
  providers: [
    {
      provider_id: { // id provider
        type: String,
        unique: true
      },
      provider: String,
      email: String, // for privider as google or facebook etc...
      displayName: String, // for privider as google or facebook etc...
      name: Object, // for privider as google or facebook etc...
      nickname: String, // for privider as google or facebook etc...
      photo: String, // for privider as google or facebook etc...
      url: String, // for privider as google or facebook etc...
      status: Boolean, // status providers enable or disabled (true|false)
      device: Object, // info about device 
      createAt: { // date where the provider was created
        type: Date,
        default: Date.now
      }
    }
  ],
  lastConnetions: {
    type: Date,
    default: Date.now
  }, // last connection
  joined: { // date where the users was created
    type: Date,
    default: Date.now
  },
  createdAt: { 
    type: Date,
    default: Date.now
  }
});

UserSchema.pre('save', async function(next) {
  const user = this;
  
  if(this.password)
    this.password = await bcrypt.hash(user.password, 8);
  else 
    this.password = null;
  
  next();
});

UserSchema.methods.isValidPassword = async function(password) {
  const user = this;
  
  const compare = await bcrypt.compare(password, user.password);

  return compare;
}

export default mongoose.model('users', UserSchema);

