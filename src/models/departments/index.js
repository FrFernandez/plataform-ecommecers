import { db } from '../../utils';

const Schema = db.Schema;

export default db.model('departaments', new Schema({
	name: String
}));