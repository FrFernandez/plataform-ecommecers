import express from 'express';
import passport from 'passport';
import schortid from 'shortid';
import jwt from 'jsonwebtoken';
import config from '../../../../config';
import { Auth } from '../../../objects';
import { Uploads } from '../../../utils';
import { LoginStrategy } from '../../../middleware';

const auth = new Auth();
const uploads = new Uploads();
const Router = express.Router();

const files = [
  {
    name: 'photo',
    maxCount: 1
  }, {
    name: 'picture',
    maxCount: 1
  }, {
    name: 'cover',
    maxCount: 1
  }
];

Router.route('/login')
        .post(LoginStrategy, async (req, res) => {
          const { _id, email } = req.user;

          // if (req.remeber)          

          let body = {
            provider_id: schortid.generate(),
            provider: 'local',
            status: true,
            maxAge: 7 * 24 * 60 * 60 * 1000,
            joined: Date.now(),
            device: {
              agent:  req.header('user-agent'),
              referrer: req.header('referrer'),
              ip: req.ip || req.ips || req.header('x-forwarded-for') || req.connection.remoteAddress
            }
          }

          const user = await auth.update(
            { _id }, 
            { 
              $push: { providers: body }, 
              online: true, 
              lastConnetions: Date.now() 
            }
          );

          const token = await jwt.sign(
            {
              _id, 
              email, 
              provider_id: body.provider_id, 
              provider: body.provider 
            }, 
            config.hashEncrypt,
            {
              maxAge: body.maxAge
            }
          );

          res.json({ user, token });
        });
Router.route('/google')
      .get(passport.authenticate('google', {
        scope: ['profile', 'email']
      }))
Router.route('/google/redirect')
      .get(passport.authenticate('google'), (req, res) => {
        return res.redirect('/users');
      })
Router.route('/register')
      .post((req, res) => {
        let body  = req.body;

        
      })
Router.route('/logout')
      // .get(passport.authenticate('jwt', { session: false }), async (req, res) => {
      //   let { token } = req.query;
      //   let user  = req.user;
        
      //   token = jwt.verify(token, 'token');

      //   try {
      //     await auth.update({ _id: user._id, "providers.provider_id": token.user.provider_id }, { $set: { "providers.$.status": false, online: false } });
      //   } catch(err) {
      //     return res.status(500).json(err);    
      //   }
        
      //   req.logout();
      //   return res.json('Logout Success');   
      // })

export default Router;