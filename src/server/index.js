import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import favicon from 'serve-favicon';
import logger from 'morgan';
import passport from 'passport';

import { AuthStrategy, GoogleStrategy, deserializeUser, serializeUser } from '../middleware';

import { Auth, Users } from './routes';

const app = express();

app.set('trust proxy', true);

passport.serializeUser(serializeUser);
passport.deserializeUser(deserializeUser);
passport.use(GoogleStrategy);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));

app.use(AuthStrategy);
app.use(passport.initialize());

app.use('/auth', Auth);
app.use('/users', Users);

export default app;

